//package com.example.ble_connection;
//
//import android.app.Service;
//import android.bluetooth.BluetoothAdapter;
//import android.bluetooth.BluetoothGatt;
//import android.bluetooth.BluetoothGattCallback;
//import android.bluetooth.BluetoothGattCharacteristic;
//import android.bluetooth.BluetoothManager;
//import android.bluetooth.BluetoothProfile;
//import android.content.Intent;
//import android.os.IBinder;
//import android.util.Log;
//
//import androidx.annotation.Nullable;
//
//import java.util.UUID;
//
//// A service that interacts with the BLE device via the Android BLE API.
//public class BluetoothLeService extends Service {
//    private final static String TAG = BluetoothLeService.class.getSimpleName();
//
//    private BluetoothManager bluetoothManager;
//    private BluetoothAdapter bluetoothAdapter;
//    private String bluetoothDeviceAddress;
//    private BluetoothGatt bluetoothGatt;
//    private int connectionState = STATE_DISCONNECTED;
//
//    private static final int STATE_DISCONNECTED = 0;
//    private static final int STATE_CONNECTING = 1;
//    private static final int STATE_CONNECTED = 2;
//    public final static String ACTION_GATT_CONNECTED =
//            "com.example.bluetooth.le.ACTION_GATT_CONNECTED";
//    public final static String ACTION_GATT_DISCONNECTED =
//            "com.example.bluetooth.le.ACTION_GATT_DISCONNECTED";
//    public final static String ACTION_GATT_SERVICES_DISCOVERED =
//            "com.example.bluetooth.le.ACTION_GATT_SERVICES_DISCOVERED";
//    public final static String ACTION_DATA_AVAILABLE =
//            "com.example.bluetooth.le.ACTION_DATA_AVAILABLE";
//    public final static String EXTRA_DATA =
//            "com.example.bluetooth.le.EXTRA_DATA";
//
//    public final static UUID UUID_HEART_RATE_MEASUREMENT =
//            UUID.fromString(SampleGattAttributes.HEART_RATE_MEASUREMENT);
//
//    // Various callback methods defined by the BLE API.
//    private final BluetoothGattCallback gattCallback =
//            new BluetoothGattCallback() {
//                @Override
//                public void onConnectionStateChange(BluetoothGatt gatt, int status,
//                                                    int newState) {
//                    String intentAction;
//                    if (newState == BluetoothProfile.STATE_CONNECTED) {
//                        intentAction = ACTION_GATT_CONNECTED;
//                        connectionState = STATE_CONNECTED;
//                        broadcastUpdate(intentAction);
//                        Log.i(TAG, "Connected to GATT server.");
//                        Log.i(TAG, "Attempting to start service discovery:" +
//                                bluetoothGatt.discoverServices());
//
//                    } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
//                        intentAction = ACTION_GATT_DISCONNECTED;
//                        connectionState = STATE_DISCONNECTED;
//                        Log.i(TAG, "Disconnected from GATT server.");
//                        broadcastUpdate(intentAction);
//                    }
//                }
//
//                @Override
//                // New services discovered
//                public void onServicesDiscovered(BluetoothGatt gatt, int status) {
//                    if (status == BluetoothGatt.GATT_SUCCESS) {
//                        broadcastUpdate(ACTION_GATT_SERVICES_DISCOVERED);
//                    } else {
//                        Log.w(TAG, "onServicesDiscovered received: " + status);
//                    }
//                }
//
//                @Override
//                // Result of a characteristic read operation
//                public void onCharacteristicRead(BluetoothGatt gatt,
//                                                 BluetoothGattCharacteristic characteristic,
//                                                 int status) {
//                    if (status == BluetoothGatt.GATT_SUCCESS) {
//                        broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
//                    }
//                }
//            };
//
//    @Nullable
//}