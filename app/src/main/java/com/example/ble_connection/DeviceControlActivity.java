package com.example.ble_connection;

import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.os.Bundle;
import android.widget.Toast;

import java.util.UUID;

public class DeviceControlActivity extends AppCompatActivity {
    BluetoothGatt gatt;
    BluetoothGattCallback gattCallback =
            new BluetoothGattCallback() {

            };


    String deviceRSSI;
    UUID HEART_RATE_SERVICE_UUID = convertFromInteger(0x180D);
    UUID HEART_RATE_MEASUREMENT_CHAR_UUID = convertFromInteger(0x2A37);
    UUID HEART_RATE_CONTROL_POINT_CHAR_UUID = convertFromInteger(0x2A39);


    public UUID convertFromInteger(int i) {
        final long MSB = 0x0000000000001000L;
        final long LSB = 0x800000805f9b34fbL;
        long value = i & 0xFFFFFFFF;
        return new UUID(MSB | (value << 32), LSB);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_control);
        deviceRSSI = getIntent().getStringExtra("RSSI");
        String device = getIntent().getStringExtra("Device");
        Toast.makeText(this,"RSSI: "+deviceRSSI,Toast.LENGTH_LONG).show();

//        BluetoothDevice myDevice = BluetoothDevice.gatt = myDevice.connectGatt(this, true, gattCallback);

    }


}
