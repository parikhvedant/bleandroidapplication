package com.example.ble_connection;

import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.UUID;

import static android.bluetooth.BluetoothAdapter.STATE_CONNECTED;

public class MainActivity extends AppCompatActivity {
    BluetoothAdapter bluetoothAdapter;
    BluetoothLeScanner bluetoothLEScanner;
    int REQUEST_ENABLE_BT = 1;
    BluetoothManager bluetoothManager;
    TextView peripheralTextView;
    Button startScanningButton;
    Button stopScanningButton;

//    private UUID HEART_RATE_SERVICE_UUID = convertFromInteger(0x180D);
    private UUID HEART_RATE_SERVICE_UUID = convertFromInteger(0x1800);
    private UUID HEART_RATE_MEASUREMENT_CHAR_UUID = convertFromInteger(0x2A37);
    private UUID HEART_RATE_CONTROL_POINT_CHAR_UUID = convertFromInteger(0x2A39);
    private UUID CLIENT_CHARACTERISTIC_CONFIG_UUID = convertFromInteger(0x2902);

    boolean enabled = true;

    BluetoothGatt gatt;
    BluetoothGattCallback gattCallback =
            new BluetoothGattCallback() {
                @Override
                public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
                    Log.e("STATECHANGED",""+newState);
                    if (newState == STATE_CONNECTED){
                        gatt.discoverServices();
                    }
                }
                @Override
                public void onServicesDiscovered(BluetoothGatt gatt, int status){
                    Log.e("HEART_RATE_SERVICE_UUID","HEART_RATE_SERVICE_UUID = "+HEART_RATE_SERVICE_UUID.toString());

                    Log.e("ServicesDiscovered - ",gatt.toString()+" - Status: "+
                            status);

                    List<BluetoothGattService> servicesList = gatt.getServices();
                    for (BluetoothGattService item : servicesList) {
                        Log.e("SERVICES List - ",item.toString()+" UUID = "+item.getUuid().toString());
                    }

                    BluetoothGattService heartRateService = gatt.getService(HEART_RATE_SERVICE_UUID);
                    Log.e("Service - ",heartRateService+" - Status: "+
                            status);
                    BluetoothGattCharacteristic characteristic =
                            gatt.getService(HEART_RATE_SERVICE_UUID)
                                    .getCharacteristic(HEART_RATE_MEASUREMENT_CHAR_UUID);
                    gatt.setCharacteristicNotification(characteristic, enabled);

                    BluetoothGattDescriptor descriptor =
                            characteristic.getDescriptor(CLIENT_CHARACTERISTIC_CONFIG_UUID);

                    descriptor.setValue(
                                  BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                    gatt.writeDescriptor(descriptor);
                }
                @Override
                public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status){
                    BluetoothGattCharacteristic characteristic =
                            gatt.getService(HEART_RATE_SERVICE_UUID)
                                    .getCharacteristic(HEART_RATE_CONTROL_POINT_CHAR_UUID);
                    characteristic.setValue(new byte[]{1, 1});
                    gatt.writeCharacteristic(characteristic);
                    Log.e("CharacteristicWritten: ",characteristic.toString());
                }
                @Override
                public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {

                    Log.e("HEARTVALUE",characteristic.getValue().toString());
//                    processData(characteristic.getValue());
                }
            };

    public UUID convertFromInteger(int i) {
        final long MSB = 0x0000000000001000L;
        final long LSB = 0x800000805f9b34fbL;
        long value = i & 0xFFFFFFFF;
        return new UUID(MSB | (value << 32), LSB);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        peripheralTextView = (TextView) findViewById(R.id.PeripheralTextView);
        peripheralTextView.setMovementMethod(new ScrollingMovementMethod());

        startScanningButton = (Button) findViewById(R.id.StartScanButton);
        startScanningButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startScanning();
            }
        });

        stopScanningButton = (Button) findViewById(R.id.StopScanButton);
        stopScanningButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                stopScanning();
            }
        });
        stopScanningButton.setVisibility(View.INVISIBLE);


        // Initializes Bluetooth adapter.
        bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        bluetoothAdapter = bluetoothManager.getAdapter();
        bluetoothLEScanner = bluetoothAdapter.getBluetoothLeScanner();

        // Ensures Bluetooth is available on the device and it is enabled. If not,
        // displays a dialog requesting user permission to enable Bluetooth.
        if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
        //CHECK IF PERMISSION IS ALLOWED
    }

    // Device scan callback.
    ScanCallback leScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            String deviceName = result.getDevice().getName();
            int deviceRSSI = result.getRssi();
            BluetoothDevice device = result.getDevice();

            peripheralTextView.append("Device Name: " + deviceName + " rssi: " + deviceRSSI + "\n");

            // auto scroll for text view
            final int scrollAmount = peripheralTextView.getLayout().getLineTop(peripheralTextView.getLineCount()) - peripheralTextView.getHeight();
            // if there is no need to scroll, scrollAmount will be <=0
            if (scrollAmount > 0)
                peripheralTextView.scrollTo(0, scrollAmount);
            if(deviceName!=null&&deviceName.equalsIgnoreCase("ID115Plus HR")){
                stopScanning();
                Log.e("BLE_DEVICE", device.toString());
                custom_toast("RSSI: "+deviceRSSI,1);
                //customStartDeviceControlActivity(deviceRSSI,device);
                customConnectToDevice(deviceRSSI,device);

            }
        }
    };


    private void customConnectToDevice(int deviceRSSI, BluetoothDevice myDevice) {
        gatt = myDevice.connectGatt(this, true, gattCallback);
    }

    public void startScanning() {
        System.out.println("start scanning");
        peripheralTextView.setText("");
        startScanningButton.setVisibility(View.INVISIBLE);
        stopScanningButton.setVisibility(View.VISIBLE);
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                bluetoothLEScanner.startScan(leScanCallback);
            }
        });
    }

    public void stopScanning() {
        System.out.println("stopping scanning");
        peripheralTextView.append("Stopped Scanning");
        startScanningButton.setVisibility(View.VISIBLE);
        stopScanningButton.setVisibility(View.INVISIBLE);
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                bluetoothLEScanner.stopScan(leScanCallback);
            }
        });
    }

    private void custom_toast(String str, int duration){
        Toast.makeText(this,str,duration).show();
    }

    private void customStartDeviceControlActivity(int deviceRSSI, BluetoothDevice device){
        Intent myIntent = new Intent(this,DeviceControlActivity.class);
        myIntent.putExtra("RSSI","" + deviceRSSI);
        myIntent.putExtra("Device", device.toString());
        startActivity(myIntent);
    }
}



//STUFF